package com.example.rapidfood;

import android.content.Intent;
import android.os.Bundle;
//import android.support.v7.app.AppCompatActivity;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import java.util.ArrayList;
import java.util.List;

public class MenuComida extends AppCompatActivity {

    TextView tv_cant2, tv_cant3, tv_cant4, tv_cant5, tv_cant6, tv_cant7, tv_cant8, tv_TotalCompra, tv_direccion;

    public List<Integer> precios = new ArrayList<>(7);

    private int cantidad2 =0, cantidad3 =0, cantidad4 =0, cantidad5 =0, cantidad6 =0, cantidad7 =0, cantidad8 =0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_comida);

        tv_cant2 = findViewById(R.id.tv_cant2);
        tv_cant3 = findViewById(R.id.tv_cant3);
        tv_cant4 = findViewById(R.id.tv_cant4);
        tv_cant5 = findViewById(R.id.tv_cant5);
        tv_cant6 = findViewById(R.id.tv_cant6);
        tv_cant7 = findViewById(R.id.tv_cant7);
        tv_cant8 = findViewById(R.id.tv_cant8);
        tv_TotalCompra = findViewById(R.id.tv_TotalCompra);
        tv_direccion = findViewById(R.id.tv_direccion);

        /* Aquí van los escuchadores de los botones de agregar y restar*/
        ImageButton btnMinus2 = (ImageButton) findViewById(R.id.imageButton5);
        ImageButton btnPlus2 = (ImageButton) findViewById(R.id.imageButton6);
        ImageButton btnMinus3 = (ImageButton) findViewById(R.id.imageButton7);
        ImageButton btnPlus3 = (ImageButton) findViewById(R.id.imageButton8);
        ImageButton btnMinus4 = (ImageButton) findViewById(R.id.imageButton9);
        ImageButton btnPlus4 = (ImageButton) findViewById(R.id.imageButton10);
        ImageButton btnMinus5 = (ImageButton) findViewById(R.id.imageButton11);
        ImageButton btnPlus5 = (ImageButton) findViewById(R.id.imageButton12);
        ImageButton btnMinus6 = (ImageButton) findViewById(R.id.imageButton13);
        ImageButton btnPlus6 = (ImageButton) findViewById(R.id.imageButton14);
        ImageButton btnMinus7 = (ImageButton) findViewById(R.id.imageButton15);
        ImageButton btnPlus7 = (ImageButton) findViewById(R.id.imageButton16);
        ImageButton btnMinus8 = (ImageButton) findViewById(R.id.imageButton17);
        ImageButton btnPlus8 = (ImageButton) findViewById(R.id.imageButton18);

        btnMinus2.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if(cantidad2!=0){
                    cantidad2-=1;
                }
                tv_cant2.setText(""+cantidad2);
                calcularTotales();
            }
        }); // calling onClick() method
        btnPlus2.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                cantidad2+=1;
                tv_cant2.setText(""+cantidad2);
                calcularTotales();
            }
        });
        btnMinus3.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if(cantidad3!=0){
                    cantidad3-=1;
                }
                tv_cant3.setText(""+cantidad3);
                calcularTotales();
            }
        });
        btnPlus3.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                cantidad3+=1;
                tv_cant3.setText(""+cantidad3);
                calcularTotales();
            }
        });
        btnMinus4.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if(cantidad4!=0){
                    cantidad4-=1;
                }
                tv_cant4.setText(""+cantidad4);
                calcularTotales();
            }
        });
        btnPlus4.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                cantidad4+=1;
                tv_cant4.setText(""+cantidad4);
                calcularTotales();
            }
        });
        btnMinus5.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if(cantidad5!=0){
                    cantidad5-=1;
                }
                tv_cant5.setText(""+cantidad5);
                calcularTotales();
            }
        });
        btnPlus5.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                cantidad5+=1;
                tv_cant5.setText(""+cantidad5);
                calcularTotales();
            }
        });
        btnMinus6.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if(cantidad6!=0){
                    cantidad6-=1;
                }
                tv_cant6.setText(""+cantidad6);
                calcularTotales();
            }
        });
        btnPlus6.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                cantidad6+=1;
                tv_cant6.setText(""+cantidad6);
                calcularTotales();
            }
        });
        btnMinus7.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if(cantidad7!=0){
                    cantidad7-=1;
                }
                tv_cant7.setText(""+cantidad7);
                calcularTotales();
            }
        });
        btnPlus7.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                cantidad7+=1;
                tv_cant7.setText(""+cantidad7);
                calcularTotales();
            }
        });
        btnMinus8.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if(cantidad8!=0){
                    cantidad8-=1;
                }
                tv_cant8.setText(""+cantidad8);
                calcularTotales();
            }
        });
        btnPlus8.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                cantidad8+=1;
                tv_cant8.setText(""+cantidad8);
                calcularTotales();
            }
        });

        precios.add(3000);
        precios.add(7000);
        precios.add(3000);
        precios.add(6000);
        precios.add(7000);
        precios.add(6000);
        precios.add(1800);

        Intent desdeLogin = getIntent();
        Bundle extras = desdeLogin.getExtras();
        if (extras != null) {
            String direccionUsuario = extras.getString("direccion");
            tv_direccion.setText(direccionUsuario);
        }

    }

    public void calcularTotales(){

        int Total = 0;
        Total+= precios.get(0)*cantidad2+precios.get(1)*cantidad3+precios.get(2)*cantidad4+precios.get(3)*cantidad5+precios.get(4)*cantidad6+precios.get(5)*cantidad7+precios.get(6)*cantidad8;
        tv_TotalCompra.setText("$"+Total);
    }

    public void hacerPedido(View view){

        Intent menuPrincipal = new Intent(this, MenuPrincipal.class);
        menuPrincipal.putExtra("direccion",tv_direccion.getText().toString());
        menuPrincipal.putExtra("total",tv_TotalCompra.getText().toString());
        startActivity(menuPrincipal);

    }

    public void eliminarPedido(View view){

        cantidad2=0;
        tv_cant2.setText(""+cantidad2);
        cantidad3=0;
        tv_cant3.setText(""+cantidad3);
        cantidad4=0;
        tv_cant4.setText(""+cantidad4);
        cantidad5=0;
        tv_cant5.setText(""+cantidad5);
        cantidad6=0;
        tv_cant6.setText(""+cantidad6);
        cantidad7=0;
        tv_cant7.setText(""+cantidad7);
        cantidad8=0;
        tv_cant8.setText(""+cantidad8);
        calcularTotales();

    }

    public void onButtonShowPopupWindowClickP2(View view) {

        // inflate the layout of the popup window
        LayoutInflater inflater = (LayoutInflater)
                getSystemService(LAYOUT_INFLATER_SERVICE);
        View popupView = inflater.inflate(R.layout.producto2, null);

        // create the popup window
        int width = LinearLayout.LayoutParams.WRAP_CONTENT;
        int height = LinearLayout.LayoutParams.WRAP_CONTENT;
        boolean focusable = true; // lets taps outside the popup also dismiss it
        final PopupWindow popupWindow = new PopupWindow(popupView, width, height, focusable);

        // show the popup window
        // which view you pass in doesn't matter, it is only used for the window tolken
        popupWindow.showAtLocation(view, Gravity.CENTER, 0, 0);

        // dismiss the popup window when touched
        popupView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                popupWindow.dismiss();
                return true;
            }
        });
    }

    public void onButtonShowPopupWindowClickP3(View view) {

        // inflate the layout of the popup window
        LayoutInflater inflater = (LayoutInflater)
                getSystemService(LAYOUT_INFLATER_SERVICE);
        View popupView = inflater.inflate(R.layout.producto3, null);

        // create the popup window
        int width = LinearLayout.LayoutParams.WRAP_CONTENT;
        int height = LinearLayout.LayoutParams.WRAP_CONTENT;
        boolean focusable = true; // lets taps outside the popup also dismiss it
        final PopupWindow popupWindow = new PopupWindow(popupView, width, height, focusable);

        // show the popup window
        // which view you pass in doesn't matter, it is only used for the window tolken
        popupWindow.showAtLocation(view, Gravity.CENTER, 0, 0);

        // dismiss the popup window when touched
        popupView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                popupWindow.dismiss();
                return true;
            }
        });
    }

    public void onButtonShowPopupWindowClickP4(View view) {

        // inflate the layout of the popup window
        LayoutInflater inflater = (LayoutInflater)
                getSystemService(LAYOUT_INFLATER_SERVICE);
        View popupView = inflater.inflate(R.layout.producto4, null);

        // create the popup window
        int width = LinearLayout.LayoutParams.WRAP_CONTENT;
        int height = LinearLayout.LayoutParams.WRAP_CONTENT;
        boolean focusable = true; // lets taps outside the popup also dismiss it
        final PopupWindow popupWindow = new PopupWindow(popupView, width, height, focusable);

        // show the popup window
        // which view you pass in doesn't matter, it is only used for the window tolken
        popupWindow.showAtLocation(view, Gravity.CENTER, 0, 0);

        // dismiss the popup window when touched
        popupView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                popupWindow.dismiss();
                return true;
            }
        });
    }

    public void onButtonShowPopupWindowClickP5(View view) {

        // inflate the layout of the popup window
        LayoutInflater inflater = (LayoutInflater)
                getSystemService(LAYOUT_INFLATER_SERVICE);
        View popupView = inflater.inflate(R.layout.producto5, null);

        // create the popup window
        int width = LinearLayout.LayoutParams.WRAP_CONTENT;
        int height = LinearLayout.LayoutParams.WRAP_CONTENT;
        boolean focusable = true; // lets taps outside the popup also dismiss it
        final PopupWindow popupWindow = new PopupWindow(popupView, width, height, focusable);

        // show the popup window
        // which view you pass in doesn't matter, it is only used for the window tolken
        popupWindow.showAtLocation(view, Gravity.CENTER, 0, 0);

        // dismiss the popup window when touched
        popupView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                popupWindow.dismiss();
                return true;
            }
        });
    }

    public void onButtonShowPopupWindowClickP6(View view) {

        // inflate the layout of the popup window
        LayoutInflater inflater = (LayoutInflater)
                getSystemService(LAYOUT_INFLATER_SERVICE);
        View popupView = inflater.inflate(R.layout.producto6, null);

        // create the popup window
        int width = LinearLayout.LayoutParams.WRAP_CONTENT;
        int height = LinearLayout.LayoutParams.WRAP_CONTENT;
        boolean focusable = true; // lets taps outside the popup also dismiss it
        final PopupWindow popupWindow = new PopupWindow(popupView, width, height, focusable);

        // show the popup window
        // which view you pass in doesn't matter, it is only used for the window tolken
        popupWindow.showAtLocation(view, Gravity.CENTER, 0, 0);

        // dismiss the popup window when touched
        popupView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                popupWindow.dismiss();
                return true;
            }
        });
    }

    public void onButtonShowPopupWindowClickP7(View view) {

        // inflate the layout of the popup window
        LayoutInflater inflater = (LayoutInflater)
                getSystemService(LAYOUT_INFLATER_SERVICE);
        View popupView = inflater.inflate(R.layout.producto7, null);

        // create the popup window
        int width = LinearLayout.LayoutParams.WRAP_CONTENT;
        int height = LinearLayout.LayoutParams.WRAP_CONTENT;
        boolean focusable = true; // lets taps outside the popup also dismiss it
        final PopupWindow popupWindow = new PopupWindow(popupView, width, height, focusable);

        // show the popup window
        // which view you pass in doesn't matter, it is only used for the window tolken
        popupWindow.showAtLocation(view, Gravity.CENTER, 0, 0);

        // dismiss the popup window when touched
        popupView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                popupWindow.dismiss();
                return true;
            }
        });
    }

    public void onButtonShowPopupWindowClickP8(View view) {

        // inflate the layout of the popup window
        LayoutInflater inflater = (LayoutInflater)
                getSystemService(LAYOUT_INFLATER_SERVICE);
        View popupView = inflater.inflate(R.layout.producto8, null);

        // create the popup window
        int width = LinearLayout.LayoutParams.WRAP_CONTENT;
        int height = LinearLayout.LayoutParams.WRAP_CONTENT;
        boolean focusable = true; // lets taps outside the popup also dismiss it
        final PopupWindow popupWindow = new PopupWindow(popupView, width, height, focusable);

        // show the popup window
        // which view you pass in doesn't matter, it is only used for the window tolken
        popupWindow.showAtLocation(view, Gravity.CENTER, 0, 0);

        // dismiss the popup window when touched
        popupView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                popupWindow.dismiss();
                return true;
            }
        });
    }

}