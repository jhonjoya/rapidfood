package com.example.rapidfood;

import androidx.annotation.NonNull;

import com.google.firebase.database.IgnoreExtraProperties;

@IgnoreExtraProperties
class User {

    private String nombreUsuario;
    private String passwordUsuario;
    private String direccionUsuario;
    private String idUsuario;

    public User() {

    }

    public User(String username, String password, String direccion, String id) {
        this.nombreUsuario = username;
        this.passwordUsuario = password;
        this.direccionUsuario = direccion;
        this.idUsuario = id;
    }


    public String getNombreUsuario() {
        return nombreUsuario;
    }

    public void setNombreUsuario(String nombreUsuario) {
        this.nombreUsuario = nombreUsuario;
    }

    public String getPasswordUsuario() {
        return passwordUsuario;
    }

    public void setPasswordUsuario(String passwordUsuario) {
        this.passwordUsuario = passwordUsuario;
    }

    public String getDireccionUsuario() {
        return direccionUsuario;
    }

    public void setDireccionUsuario(String direccionUsuario) {
        this.direccionUsuario = direccionUsuario;
    }

    public String getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(String idUsuario) {
        this.idUsuario = idUsuario;
    }

    @Override
    public String toString() {
        return "User{" +
                "nombreUsuario='" + nombreUsuario + '\'' +
                ", passwordUsuario='" + passwordUsuario + '\'' +
                ", direccionUsuario='" + direccionUsuario + '\'' +
                ", idUsuario='" + idUsuario + '\'' +
                '}';
    }
}
