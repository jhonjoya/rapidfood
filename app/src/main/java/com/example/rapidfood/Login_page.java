package com.example.rapidfood;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
//import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Objects;

public class Login_page extends AppCompatActivity {

    private EditText et_user, et_password;

    List<User> userList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_page);

        et_user = findViewById(R.id.et_user);
        et_password = findViewById(R.id.et_password);

    }

    public void irANuevoRegistro (View view){
        Intent nuevoRegistro = new Intent(this, NuevoUsuario.class);
        startActivity(nuevoRegistro);
    }

    public void credenciales(View view) {

        String usuariolog = et_user.getText().toString();
        String contrasenalog = et_password.getText().toString();
        User userLog = new User();
        userLog.setIdUsuario(usuariolog);
        userLog.setPasswordUsuario(contrasenalog);

        if(!usuariolog.matches("") && !contrasenalog.matches("")){
            Query query = FirebaseDatabase.getInstance().getReference("usuarios").orderByChild("nombreUsuario").equalTo(usuariolog);
            query.get().addOnCompleteListener(task -> {
                if (!task.isSuccessful()) {
                    Log.e("firebase", "Error getting data", task.getException());
                }
                else {
                    for (DataSnapshot snapshot: task.getResult().getChildren()) {
                        User usuario = snapshot.getValue(User.class);
                        userList.add(usuario);
                    }

                    if (!userList.isEmpty()){
                        if (userList.get(0).getPasswordUsuario().equals(userLog.getPasswordUsuario())){
                            Toast mensaje = Toast.makeText(this, "Usuario encontrado", Toast.LENGTH_LONG);
                            mensaje.show();
                            Intent menuComida = new Intent(this, MenuComida.class);
                            menuComida.putExtra("direccion",userList.get(0).getDireccionUsuario());
                            startActivity(menuComida);
                        }
                        else {
                            Toast errorlog = Toast.makeText(this, "usuario o contraseña incorrecta", Toast.LENGTH_LONG);
                            errorlog.show();
                        }
                    }
                    else {
                        Toast errorlog = Toast.makeText(this, "usuario o contraseña incorrecta", Toast.LENGTH_LONG);
                        errorlog.show();
                    }
                }
            });

        } else {
            Toast error = Toast.makeText(this, "llene los campos vacíos", Toast.LENGTH_LONG);
            error.show();
        }

    }

}