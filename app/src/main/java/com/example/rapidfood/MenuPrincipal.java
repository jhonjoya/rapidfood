package com.example.rapidfood;

import android.content.Intent;
import android.os.Bundle;
//import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

public class MenuPrincipal extends AppCompatActivity {

    TextView tv_TotalVenta, tv_direccionCompra;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_principal);

        tv_TotalVenta = findViewById(R.id.tv_TotalVenta);
        tv_direccionCompra = findViewById(R.id.tv_direccionCompra);

        Intent desdeMenuComida = getIntent();
        Bundle extras = desdeMenuComida.getExtras();
        if (extras != null) {
            String direccionUsuario = extras.getString("direccion");
            String totalCompra = extras.getString("total");
            tv_direccionCompra.setText(direccionUsuario);
            tv_TotalVenta.setText(totalCompra);
        }
    }

    public void nuevaCompra (View view){

        Intent newbuy = new Intent(this, MenuComida.class);
        startActivity(newbuy);

    }

}