package com.example.rapidfood;

import android.content.Intent;
import android.os.Bundle;
//import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {

    private TextView tv_welcome;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        tv_welcome = findViewById(R.id.tv_welcome);

    }

    public void pasarLogueo (View view){

        Intent i = new Intent(this, Login_page.class);
        startActivity(i);

    }

}