package com.example.rapidfood;

import android.content.ContentValues;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class NuevoUsuario extends AppCompatActivity {

    private EditText et_newUser, et_newPass, et_newAdd;

    FirebaseDatabase database = FirebaseDatabase.getInstance();
    DatabaseReference myRef = database.getReference("usuarios");

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nuevo_usuario);

        et_newUser = findViewById(R.id.et_newUser);
        et_newPass = findViewById(R.id.et_newPass);
        et_newAdd = findViewById(R.id.et_newAdd);

    }

    public void crearRegistro(View view) {

        String nuevoUsuario = et_newUser.getText().toString();
        String nuevoPassword = et_newPass.getText().toString();
        String nuevaDireccion = et_newAdd.getText().toString();

        if(!nuevoUsuario.matches("") && !nuevoPassword.matches("") && !nuevaDireccion.matches("")){

            String mUId = myRef.push().getKey();
            User user = new User(nuevoUsuario, nuevoPassword, nuevaDireccion, mUId);

            myRef.child(mUId).setValue(user);
            //mDatabase.child("users").child(userId).child("username").setValue(name);

            AdminSQLiteOpenHelper admin = new AdminSQLiteOpenHelper(this,
                    "administracion", null, 1);

            SQLiteDatabase bd = admin.getWritableDatabase();

            ContentValues registro = new ContentValues();
            registro.put("usuario", nuevoUsuario);
            registro.put("contrasena", nuevoPassword);
            registro.put("direccion", nuevaDireccion);
            bd.insert("usuarios", null, registro);
            bd.close();

            et_newUser.setText("");
            et_newPass.setText("");
            et_newAdd.setText("");
            Toast mensaje = Toast.makeText(this, "Se cargaron los datos del nuevo usuario",
                    Toast.LENGTH_LONG);
            mensaje.show();

            Intent loginPage = new Intent(this, Login_page.class);
            startActivity(loginPage);

        }
        else{
            Toast error =Toast.makeText(this, "Debe llenar el usuario y contraseña",
                    Toast.LENGTH_LONG);
            error.show();
        }

    }
}