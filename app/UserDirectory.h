//
// Created by Jhon on 11/29/2021.
//

#ifndef RAPIDFOOD_USERDIRECTORY_H
#define RAPIDFOOD_USERDIRECTORY_H



public class UserDirectory {

    private String username;
    private String password;

    public UserDirectory(String name, String pass){
        this.username = name;
        this.password = pass;
    }

    public String getUsername(){
        return username;
    }

    public String getPassword(){
            return password;
        }

};



#endif //RAPIDFOOD_USERDIRECTORY_H
